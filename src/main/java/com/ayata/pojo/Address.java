package com.ayata.pojo;

import lombok.Data;

@Data
public class Address {
    String id;
    String ref;
    String type;
    String timeZone;
    String companyName;
    String name;
    String street;
    String city;
    String postcode;
    String state;
    String region;
    String country;
    Float latitude;
    Float longitude;
}
