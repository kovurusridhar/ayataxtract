package com.ayata.pojo;

import java.util.List;

public class Article {
    String id;
    String ref;
    String type;
    String status;
    List<Attribute> attributes;
    String description;
    float height;
    float length;
    String name;
    float weight;
    float width;
    int quantity;
    FulfilmentConnection fulfilments;
}
