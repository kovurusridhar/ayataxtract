package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class ArticleConnection {
    List<ArticleEdge> edges;
}
