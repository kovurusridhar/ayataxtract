package com.ayata.pojo;

import lombok.Data;

@Data
public class ArticleEdge {
    Article node;
}
