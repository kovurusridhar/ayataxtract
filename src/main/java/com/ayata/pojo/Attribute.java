package com.ayata.pojo;

import lombok.Data;

@Data
public class Attribute {
    String name;
    String type;
    Object value;
}
