package com.ayata.pojo;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class AttributeTypeAdopter extends TypeAdapter<Attribute> {
    @Override
    public Attribute read(final JsonReader in) throws IOException {
        final Attribute attribute = new Attribute();
        while (in.hasNext()) {
            String fieldName = in.nextName();
            if(fieldName.equalsIgnoreCase("attributes")){
                String attrSt = in.nextString();
                System.out.println(attrSt);
            }

            switch (in.nextName()) {
                case "name":
                    attribute.setName(in.nextString());
                    break;
                case "title":
                    attribute.setType(in.nextString());
                    break;
                case "authors":
                    //attribute.setValue(in.nextString());
                    break;
            }
        }
        in.endObject();
        return attribute;

    }
    @Override
    public void write(final JsonWriter out, final Attribute attribute) throws IOException {
        out.beginObject();
        out.name("name").value(attribute.getName());
        out.name("type").value(attribute.getType());
        //out.name("value").value(attribute.getValue().toString());
        out.endObject();
    }
    }
