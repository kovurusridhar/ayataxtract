package com.ayata.pojo;

import lombok.Data;

import java.lang.annotation.Retention;

@Data
public class Carrier {
    String id;
    String name;
    String type;
    String ref;
    Retailer retailer;

}
