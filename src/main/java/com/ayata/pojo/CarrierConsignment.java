package com.ayata.pojo;

public class CarrierConsignment {
    String id;
    String ref;
    String status;
    String consignmentReference;
    String trackingLabel;
    String labelUrl;
    String orderSummaryUrl;
    CarrierConsignmentArticleConnection carrierConsignmentArticles;
}
