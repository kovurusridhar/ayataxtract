package com.ayata.pojo;

import lombok.Data;

@Data
public class CarrierConsignmentArticle {
    Article article;
    CarrierConsignment carrierConsignment;
}
