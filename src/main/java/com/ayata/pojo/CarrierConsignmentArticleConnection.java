package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class CarrierConsignmentArticleConnection
{
    List<CarrierConsignmentArticleEdge> edges;
    PageInfo pageInfo;

}
