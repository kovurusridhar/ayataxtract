package com.ayata.pojo;

import lombok.Data;

@Data
public class CarrierConsignmentArticleEdge {
    CarrierConsignmentArticle node;
}
