package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class CarrierConsignmentConnection {
    List<CarrierConsignmentEdge> edges;
    PageInfo pageInfo;

}
