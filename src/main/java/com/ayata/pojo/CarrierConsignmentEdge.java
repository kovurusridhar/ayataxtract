package com.ayata.pojo;

import lombok.Data;

@Data
public class CarrierConsignmentEdge {
    CarrierConsignment node;
}
