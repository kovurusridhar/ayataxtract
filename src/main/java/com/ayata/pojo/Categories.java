package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Categories {
    List<CategoryEdge> edges;
    PageInfo pageInfo;
}
