package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Category {
    String createdOn;
    String updatedOn;
    String id;
    String ref;
    String status;
    String type;
    List<Attribute> attributes;
    String name;
    String summary;
    Category parentCategory;
    CategoryConnection childCategories;
}
