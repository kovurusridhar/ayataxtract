package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class CategoryConnection {
    List<CategoryEdge> edges;
    PageInfo pageInfo;
}
