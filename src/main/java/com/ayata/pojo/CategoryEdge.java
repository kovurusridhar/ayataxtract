package com.ayata.pojo;

import lombok.Data;

@Data
public class CategoryEdge {
    Category node;
    String cursor;
}
