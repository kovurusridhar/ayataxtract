package com.ayata.pojo;

import lombok.Data;

@Data
public class Consignment {
    String id;
    String ref;
    String status;
    String consignmentReference;
    String trackingLabel;
    String labelUrl;
    String orderSummaryUrl;
    ConsignmentArticleConnection consignmentArticles;
    Carrier carrier;
}
