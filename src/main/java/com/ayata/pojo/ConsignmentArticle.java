package com.ayata.pojo;

import lombok.Data;

@Data
public class ConsignmentArticle {
    Article article;
    Consignment consignment;
}
