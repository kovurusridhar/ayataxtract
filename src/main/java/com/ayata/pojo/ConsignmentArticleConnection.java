package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class ConsignmentArticleConnection {
    List<ConsignmentArticleEdge> edges;
    PageInfo pageInfo;

}
