package com.ayata.pojo;

import lombok.Data;

@Data
public class ConsignmentArticleEdge {
    ConsignmentArticle node;
}
