package com.ayata.pojo;

import lombok.Data;

@Data
public class Customer {
    String id;
    String ref;
    String firstName;
    String lastName;
    String title;
    String username;
    String primaryEmail;
    boolean promotionOptIn;

}
