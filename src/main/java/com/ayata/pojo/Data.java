package com.ayata.pojo;

@lombok.Data
public class Data {
    Orders orders;
    Fulfilments fulfilments;
    InventoryPositions inventoryPositions;
    VariantProducts variantProducts;
    StandardProducts standardProducts;
    VirtualPositions  virtualPositions;
    Locations locations;
    Categories categories;



}
