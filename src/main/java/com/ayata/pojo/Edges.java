package com.ayata.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL) 	//  ignore all null fields

public class Edges {
    String cursor;
    OrderNode node;
}
