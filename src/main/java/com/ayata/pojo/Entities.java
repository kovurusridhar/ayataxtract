package com.ayata.pojo;


public class Entities {
    String locationRef;
    int qty;
    int correctedQty;
    int reserveQty;
    String skuRef;
    int retailerId;

    public String getLocationRef() {
        return locationRef;
    }

    public void setLocationRef(String locationRef) {
        this.locationRef = locationRef;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getCorrectedQty() {
        return correctedQty;
    }

    public void setCorrectedQty(int correctedQty) {
        this.correctedQty = correctedQty;
    }

    public int getReserveQty() {
        return reserveQty;
    }

    public void setReserveQty(int reserveQty) {
        this.reserveQty = reserveQty;
    }

    public String getSkuRef() {
        return skuRef;
    }

    public void setSkuRef(String skuRef) {
        this.skuRef = skuRef;
    }

    public int getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(int retailerId) {
        this.retailerId = retailerId;
    }

    @Override
    public String toString() {
        return "{" +
                "\"locationRef\":\"" + locationRef + '\"' +
                ", \"qty\":" + qty +
                ", \"correctedQty\":" + correctedQty +
                ", \"reserveQty\":" + reserveQty +
                ", \"skuRef\":\"" + skuRef + '\"' +
                ", \"retailerId\":" + retailerId +
                '}';
    }
}

