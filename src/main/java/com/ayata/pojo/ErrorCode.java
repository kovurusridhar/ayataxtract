package com.ayata.pojo;

import lombok.Data;

@Data
public class ErrorCode {
    String  code;
    String  errorMessage;

}
