package com.ayata.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class FinancialTransaction {
    String id;
    String ref;
    String status;
    String type;
    Date createdOn;
    Date updatedOn;
    Float total;
    String currency;
    String externalTransactionCode;
    String externalTransactionId;
    String cardType;
    String paymentMethod;
    String paymentProviderName;
}
