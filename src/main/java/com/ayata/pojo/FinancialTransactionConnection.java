package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class FinancialTransactionConnection {
    List<FinancialTransactionEdge> edges;
    PageInfo pageInfo;

}
