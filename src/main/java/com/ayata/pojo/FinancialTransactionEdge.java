package com.ayata.pojo;

import lombok.Data;

@Data
public class FinancialTransactionEdge {
    FinancialTransaction node;

}
