package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class FinancialTransactions {
    List<FinancialTransactionEdge> edges;
}
