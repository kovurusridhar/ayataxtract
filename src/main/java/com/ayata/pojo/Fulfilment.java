package com.ayata.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Fulfilment {
    String id;
    String ref;
    String status;
    String deliveryType;
    String type;
    String eta;
    Date expiryTime;
    Order order;
    Address fromAddress;
    Address toAddress;
    List<FulfilmentItem> items;
    List<Article> articles;

}
