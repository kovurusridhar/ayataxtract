package com.ayata.pojo;

import lombok.Data;

@Data
public class FulfilmentChoice {
    String id;
    String currency;
    String deliveryInstruction;
    String deliveryType;
    String fulfilmentPrice;
    String fulfilmentTaxPrice;
    String fulfilmentType;
    String pickupLocationRef;
    Address deliveryAddress;

}
