package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class FulfilmentConnection {
    List<FulfilmentEdge> edges;
    PageInfo pageInfo;


}
