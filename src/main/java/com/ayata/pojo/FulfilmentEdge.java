package com.ayata.pojo;

import lombok.Data;

@Data
public class FulfilmentEdge {
    FulfilmentNode node;
    String cursor;
    FulfilmentItemConnection items;

}
