package com.ayata.pojo;

import lombok.Data;

@Data
public class FulfilmentEdges {
    String cursor;
    FulfilmentNode node;


}
