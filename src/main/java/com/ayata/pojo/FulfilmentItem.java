package com.ayata.pojo;

import lombok.Data;

@Data
public class FulfilmentItem {
    String id;
    String ref;
    String status;
    int requestedQuantity;
    int filledQuantity;
    int rejectedQuantity;
    Fulfilment fulfilment;
    OrderItem orderItem;
}
