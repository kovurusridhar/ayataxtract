package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class FulfilmentItemConnection {
    List<FulfilmentItemEdge> edges;
    PageInfo pageInfo;
    String cursor;
}
