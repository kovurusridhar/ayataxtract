package com.ayata.pojo;

import lombok.Data;

@Data
public class FulfilmentItemEdge {
    FulfilmentItem node;
    String cursor;

}
