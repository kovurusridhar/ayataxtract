package com.ayata.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FulfilmentNode {
    String id;
    String ref;
    String status;
    List<Attribute> attributes;
    Date createdOn;
    Date updatedOn;
    String deliveryType;
    String type;
    String eta;
    Date expiryTime;
    Order order;
    Address fromAddress;
    Address toAddress;
    Customer user;
    ArticleConnection articles;
    FulfilmentItemConnection items;



}
