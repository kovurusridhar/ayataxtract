package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Fulfilments {
    PageInfo pageInfo;
    List<FulfilmentEdge> edges;

}
