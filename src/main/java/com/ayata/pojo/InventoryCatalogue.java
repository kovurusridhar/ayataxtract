package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class InventoryCatalogue {
    String id;
    String ref;
    String status;
    List<Attribute> attributes;
    String name;
    String description;
    InventoryPositionConnection inventoryPositions;
    List<String> retailerRefs;
    InventoryQuantityConnection quantities;
}
