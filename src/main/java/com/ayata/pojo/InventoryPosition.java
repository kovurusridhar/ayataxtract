package com.ayata.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class InventoryPosition {
    String id;
    Date createdOn;
    Date updatedOn;
    String ref;
    String type;
    String status;
    List<Attribute> attributes;
    String productRef;
    int onHand;
    String locationRef;
    //InventoryCatalogue catalogue;

}
