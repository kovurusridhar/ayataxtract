package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class InventoryPositionConnection {
    List<InventoryPositionEdge> edges;
    PageInfo pageInfo;
}
