package com.ayata.pojo;

import lombok.Data;

@Data
public class InventoryPositionEdge {
    InventoryPosition node;
    String cursor;
}
