package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class InventoryPositions {
    List<InventoryPositionEdge> edges;
    PageInfo pageInfo;
}
