package com.ayata.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class InventoryQuantity {
    String id;
    String ref;
    String type;
    String status;
    int quantity;
    String condition;
    Date expectedOn;
    String storageAreaRef;
    InventoryCatalogue catalogue;
    InventoryPosition position;

}
