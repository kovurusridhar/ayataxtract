package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class InventoryQuantityConnection {
    List<InventoryQuantityEdge> edges;
    PageInfo pageInfo;
}
