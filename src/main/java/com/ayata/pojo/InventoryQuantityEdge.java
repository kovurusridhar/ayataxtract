package com.ayata.pojo;

import lombok.Data;

@Data
public class InventoryQuantityEdge {
    InventoryQuantity node;
    String cursor;
}
