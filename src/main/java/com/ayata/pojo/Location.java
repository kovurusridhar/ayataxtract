package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Location {
    String id;
    String ref;
    String status;
    List<Attribute> attributes;
}
