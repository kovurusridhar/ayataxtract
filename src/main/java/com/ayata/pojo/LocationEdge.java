package com.ayata.pojo;

import lombok.Data;

@Data
public class LocationEdge {
    Location node;
    String cursor;
}
