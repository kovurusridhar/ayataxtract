package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Locations {
    List<LocationEdge> edges;
    PageInfo pageInfo;

}
