package com.ayata.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY) 	//  ignore all null fields
public class Order {
    String createdOn;
    String updatedOn;
    String  id;
    String ref;
    String status;
    List<Attribute> attributes;
    Retailer retailer;
    Float totalPrice;
    Float totalTaxPrice;
    FulfilmentChoice fulfilmentChoice;
    List<FinancialTransactionEdge> financialTransactions;
    List<OrderItemEdge> orderItems;
    List<FulfilmentEdge> fulfilments;





}
