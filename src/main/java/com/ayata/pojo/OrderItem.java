package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class OrderItem {
    String id;
    String ref;
    Product product;
    String status;
    int quantity;
    String currency;
    Float paidPrice;
    Float totalPrice;
    Float taxPrice;
    Float taxType;
    List<Attribute> attributes;

}
