package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class OrderItemConnection {
    List<OrderItemEdge> edges;
    PageInfo pageInfo;

}
