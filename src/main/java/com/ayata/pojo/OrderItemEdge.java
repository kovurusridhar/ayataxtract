package com.ayata.pojo;

import lombok.Data;

@Data
public class OrderItemEdge {
    OrderItem node;
}
