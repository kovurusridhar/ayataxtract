package com.ayata.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL) 	//  ignore all null fields
public class OrderNode {
    String createdOn;
    String updatedOn;
    String  id;
    String ref;
    String type;
    String status;
    List<Attribute> attributes;
    Retailer retailer;
    Float totalPrice;
    Float totalTaxPrice;
    Customer customer;
    FulfilmentChoice fulfilmentChoice;
    FinancialTransactionConnection financialTransactions;
    OrderItemConnection items;
    FulfilmentConnection fulfilments;


}
