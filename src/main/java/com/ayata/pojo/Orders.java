package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Orders {
    PageInfo pageInfo;
    List<Edges> edges;

}
