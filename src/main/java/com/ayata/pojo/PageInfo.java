package com.ayata.pojo;

import lombok.Data;

@Data
public class PageInfo {
    boolean hasNextPage;

}
