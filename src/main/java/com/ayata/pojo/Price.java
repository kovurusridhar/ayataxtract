package com.ayata.pojo;

import lombok.Data;

@Data
public class Price {
    String type;
    String currency;
    String value;
}
