package com.ayata.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class ProductCatalogue {
    String id;
    String type;
    String status;
    String ref;
    String name;
    Date createdOn;
    Date updatedOn;
    String description;
}
