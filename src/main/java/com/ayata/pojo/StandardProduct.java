package com.ayata.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class StandardProduct {

    Date createdOn;
    Date updatedOn;
    String id;
    String ref;
    String status;
    String type;
    String name;
    String summary;
    CategoryConnection categories;
    List<Price> prices;
    VariantProductConnection variants;
}
