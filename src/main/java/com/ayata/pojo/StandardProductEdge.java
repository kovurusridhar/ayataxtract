package com.ayata.pojo;

import lombok.Data;

@Data
public class StandardProductEdge {
    StandardProduct node;
    String cursor;

}
