package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class StandardProducts {
    List<StandardProductEdge> edges;
    PageInfo pageInfo;
}
