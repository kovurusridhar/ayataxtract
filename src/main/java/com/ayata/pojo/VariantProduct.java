package com.ayata.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class VariantProduct {
    Date createdOn;
    Date updatedOn;
    String id;
    String ref;
    String status;
    String type;
    List<Attribute> attributes;
    StandardProduct product;
    String gtin;
    String name;
    ProductCatalogue catalogue;
    CategoryConnection categories;
    Category parentCategory;
    CategoryConnection childCategories;
    List<Price> prices;

}
