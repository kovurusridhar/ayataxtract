package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class VariantProductConnection {
    List<VariantProductEdge> edges;
    PageInfo pageInfo;
}
