package com.ayata.pojo;

import lombok.Data;

@Data
public class VariantProductEdge {

    VariantProduct node;
    String cursor;
}
