package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class VariantProducts {
    PageInfo pageInfo;
    List<VariantProductEdge> edges;
}
