package com.ayata.pojo;

import lombok.Data;

@Data
public class VirtualCatalogue {
    String id;
    String ref;
    String status;
    String createdOn;
    String updatedOn;
    String type;
    String description;
}
