package com.ayata.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class VirtualPosition {
    String id;
    String ref;
    String status;
    Date createdOn;
    Date updatedOn;
    String productRef;
    int quantity;
    String groupRef;
    VirtualCatalogue catalogue;
}
