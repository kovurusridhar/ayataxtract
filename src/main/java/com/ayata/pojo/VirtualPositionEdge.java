package com.ayata.pojo;

import lombok.Data;

@Data
public class VirtualPositionEdge {
    VirtualPosition node;
    String cursor;
}
