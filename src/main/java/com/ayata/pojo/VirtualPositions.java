package com.ayata.pojo;

import lombok.Data;

import java.util.List;

@Data
public class VirtualPositions {
    List<VirtualPositionEdge> edges;
    PageInfo pageInfo;
}
