package com.ayata.xtract;

import com.ayata.pojo.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jfoenix.controls.*;
import com.jfoenix.validation.RequiredFieldValidator;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.ayata.xtract.Constants.*;

public class AyataXtractController implements Initializable {



    @FXML
    private JFXPasswordField authToken;

    @FXML
    private JFXButton xtractButton;

    @FXML
    private JFXTextArea query;

    @FXML
    private JFXTextField hostUrl;

    @FXML
    private JFXTextField maxEntries;

    @FXML
    private JFXTextField retailerId;

    @FXML
    private JFXTextField resetValue;

    @FXML
    private JFXComboBox<String> entityType;

    @FXML
    private JFXComboBox<String> fileFormat;

    @FXML
    private JFXComboBox<String> environment;


    @FXML
    private JFXSpinner spinner;

    @FXML
    private JFXButton resetbutton;

    @FXML
    private JFXTextField downloadDir;


    private String selectedEntityType;

    private String selectedFileFormat;

    private  String selectedEnvironment;

    final String CSV_LOCATION = "Orders.csv";

    List<OrderNode> ordersList;
    List<FulfilmentNode> fulfilmentsList;
    List<InventoryPosition> inventoryPositionsList;
    List<VariantProduct> variantProductList;
    List<StandardProduct> standardProductList;
    List<VirtualPosition> virtualPositionList;
    List<Location> locationList;
    List<Category> categoryList;

    private String previousCursor;
    private String selectedDirPath;
    List<Entities> entities = new ArrayList<>();



    ObservableList<String> entityTypes = FXCollections.observableArrayList(
            "Order", "Fulfilment", "Standard Product", "Variant Product", "Inventory Position", "Virtual Position","Locations","Category","Reset Inventory Batch");

    ObservableList<String> fileFormats = FXCollections.observableArrayList(
            "CSV", "JSON");

    ObservableList<String> onlyJsonFormat = FXCollections.observableArrayList("JSON");

    ObservableList<String> environmentTypes= FXCollections.observableArrayList(
            "Sandbox", "Production");


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        spinner.setVisible(false);
        downloadDir.setVisible(false);

        entityType.getItems().addAll(entityTypes);
        entityType.setValue("");
        maxEntries.setText("");

        fileFormat.getItems().addAll(fileFormats);
        fileFormat.setValue("");

        environment.getItems().addAll(environmentTypes);
        environment.setValue("");
//        maxEntries.setVisible(false);
//        resetValue.setVisible(false);
//        retailerId.setVisible(false);
//        maxEntries.setVisible(false);
//

        query.setText(DEFAULT_TEXT);
        entityType.getSelectionModel().clearSelection();
        fileFormat.getSelectionModel().clearSelection();
        resetValue.setVisible(false);
        maxEntries.setVisible(false);
        retailerId.setVisible(false);

        entityType.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    selectedEntityType = newValue;

                    if (newValue != null) {
                        if (newValue.equalsIgnoreCase(ORDER)) {

                            query.setText("{\n" +
                                    "  orders(type: \"HD\", first:4000, after:\"\") {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        status\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");
                        }if(newValue.equalsIgnoreCase(FULFILMENT)){

                            query.setText("{\n" +
                                    "  fulfilments(first:4000, after:\"\", createdOn: {from: \"2020-02-16\", to: \"2022-01-30\"}, status: \"ASSIGNED\") {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        status\n" +
                                    "        order {\n" +
                                    "          id\n" +
                                    "        }\n" +
                                    "        type\n" +
                                    "        deliveryType\n" +
                                    "        items {\n" +
                                    "          edges {\n" +
                                    "            node {\n" +
                                    "              id\n" +
                                    "              ref\n" +
                                    "              status\n" +
                                    "              requestedQuantity\n" +
                                    "              filledQuantity\n" +
                                    "              rejectedQuantity\n" +
                                    "            }\n" +
                                    "          }\n" +
                                    "        }\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");
                        } else if (newValue.equals(STANDARD_PRODUCT)) {
                            query.setText("{\n" +
                                    "  standardProducts(first:4000,status: \"ACTIVE\", after:\"\", catalogue: {ref: \"PRODUCT_CATALOG_REF_HERE\"}) {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        type\n" +
                                    "        status\n" +
                                    "        gtin\n" +
                                    "        name\n" +
                                    "        summary\n" +
                                    "        prices {\n" +
                                    "          type\n" +
                                    "          currency\n" +
                                    "          value\n" +
                                    "        }\n" +
                                    "        variants {\n" +
                                    "          edges {\n" +
                                    "            node {\n" +
                                    "              id\n" +
                                    "              ref\n" +
                                    "              name\n" +
                                    "            }\n" +
                                    "          }\n" +
                                    "        }\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");
                        } else if (newValue.equals("Variant Product")) {
                            query.setText("{\n" +
                                    "  variantProducts(first:4000, after:\"\", status: \"ACTIVE\", catalogue: {ref: \"PRODUCT_CATALOG_REF_HERE\"}) {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      __typename\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        __typename\n" +
                                    "        createdOn\n" +
                                    "        updatedOn\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        type\n" +
                                    "        status\n" +
                                    "        gtin\n" +
                                    "        name\n" +
                                    "        summary\n" +
                                    "        prices {\n" +
                                    "          type\n" +
                                    "          currency\n" +
                                    "          value\n" +
                                    "          __typename\n" +
                                    "        }\n" +
                                    "        categories {\n" +
                                    "          edges {\n" +
                                    "            node {\n" +
                                    "              id\n" +
                                    "              ref\n" +
                                    "              name\n" +
                                    "            }\n" +
                                    "          }\n" +
                                    "        }\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");
                        } else if (newValue.equals(INVENTORY_POSITION)) {
                            query.setText("{\n" +
                                    "  inventoryPositions(catalogue: {ref: \"INVENTORY_CATALOG_REF_HERE\"}, first:4000, after:\"\") {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        status\n" +
                                    "        onHand\n" +
                                    "        productRef\n" +
                                    "        locationRef\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");
                        } else if (newValue.equalsIgnoreCase(VIRTUAL_POSITION)) {
                            query.setText("{\n" +
                                    "  virtualPositions(first:4000,after:\"\", status: \"ACTIVE\", catalogue: {ref: \"VIRTUAL_CATALOG_REF_HERE\"}) {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        status\n" +
                                    "        productRef\n" +
                                    "        quantity\n" +
                                    "        groupRef\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");

                        }else if(newValue.equalsIgnoreCase(LOCATIONS)){
                            query.setText("query {\n" +
                                    "  locations(first: 1000) {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        status\n" +
                                    "        attributes {\n" +
                                    "          name\n" +
                                    "          type\n" +
                                    "          value\n" +
                                    "        }\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");

                        }else if(newValue.equalsIgnoreCase(CATEGORY)){
                            query.setText("query {\n" +
                                    "  categories(first: 1000) {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        status\n" +
                                    "        attributes {\n" +
                                    "          name\n" +
                                    "          type\n" +
                                    "          value\n" +
                                    "        }\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");

                        }else if(newValue.equalsIgnoreCase(RESET_INVENTORY_BATCH)){
                            maxEntries.setVisible(true);
                            resetValue.setVisible(true);
                            retailerId.setVisible(true);

                            query.setText("{\n" +
                                    "  inventoryPositions(catalogue: {ref: \"INVENTORY_CATALOG_REF_HERE\"}, first:4000, after:\"\") {\n" +
                                    "    pageInfo {\n" +
                                    "      hasNextPage\n" +
                                    "    }\n" +
                                    "    edges {\n" +
                                    "      cursor\n" +
                                    "      node {\n" +
                                    "        id\n" +
                                    "        ref\n" +
                                    "        status\n" +
                                    "        onHand\n" +
                                    "        productRef\n" +
                                    "        locationRef\n" +
                                    "      }\n" +
                                    "    }\n" +
                                    "  }\n" +
                                    "}\n");


                            /*FileChooser fileChooser = new FileChooser();
                            //only allow text files to be selected using chooser
                            fileChooser.getExtensionFilters().add(
                                    new FileChooser.ExtensionFilter("Text files (*.txt)", "*.txt")
                            );
                            //set initial directory somewhere user will recognise
                            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                            //let user select file
                            File fileToLoad = fileChooser.showOpenDialog(null);
                            //if file has been chosen, load it using asynchronous method (define later)
                            String absolutePath = fileToLoad.getAbsolutePath();
                            query.setText("File selected"+absolutePath);
*/
                        }
                    }
                }
        );


    }

    @FXML
    void onChangeEntityType(ActionEvent event) {
        selectedEntityType = entityType.getSelectionModel().selectedItemProperty().get();
    }

    @FXML
    void fileFormatChanged(ActionEvent event) {

        selectedFileFormat = fileFormat.getSelectionModel().selectedItemProperty().get();
    }
    @FXML
    void environmentChanged(ActionEvent event) {
        selectedEnvironment = environment.getSelectionModel().selectedItemProperty().get();
        if("Sandbox".equalsIgnoreCase(selectedEnvironment)){
            hostUrl.setText(DEFAULT_SANBOX_HOST);
        }else if("Production".equalsIgnoreCase(selectedEnvironment)){
            hostUrl.setText(DEFAULT_PROD_HOST);
        }
    }

    private String getAllEntries(GraphQLClient graphQLClient, String graphQlQuery, JsonObject variables) throws Exception {
        JsonObject response = null;
        response = graphQLClient.execute(graphQlQuery, variables);
        String selectedItem = entityType.getSelectionModel().getSelectedItem();
        if (response != null) {
            if(ORDER.equalsIgnoreCase(selectedItem)){
                String replacedQuery = getUpdatedQueryOfOrders(graphQlQuery, response);
                if (replacedQuery != null)
                    return replacedQuery;

            }else if(FULFILMENT.equalsIgnoreCase(selectedItem)){
                String replacedQuery = getUpdatedQueryOfFulfilments(graphQlQuery, response);
                if(replacedQuery != null)
                    return  replacedQuery;

            }else if(INVENTORY_POSITION.equalsIgnoreCase(selectedItem)){
                String replacedQuery = getUpdatedQueryOfInventoryPositions(graphQlQuery, response);
                if (replacedQuery != null)
                    return replacedQuery;
            }else if(VARIANT_PRODUCT.equalsIgnoreCase(selectedItem)){
                String replacedQuery = getUpdatedQueryOfVariantProducts(graphQlQuery, response);
                if (replacedQuery != null)
                    return replacedQuery;
            }else if(STANDARD_PRODUCT.equalsIgnoreCase(selectedItem)){
                String replacedQuery = getUpdatedQueryOfStandardProducts(graphQlQuery, response);
                if (replacedQuery != null)
                    return replacedQuery;
            } else if(VIRTUAL_POSITION.equalsIgnoreCase(selectedItem)){
                String replacedQuery = getUpdatedQueryOfVirtualPositions(graphQlQuery, response);
                if (replacedQuery != null)
                    return replacedQuery;
            }else if(LOCATIONS.equalsIgnoreCase(selectedItem)){
                String replacedQuery = getUpdatedQueryOfLocations(graphQlQuery, response);
                if(replacedQuery != null){
                    return replacedQuery;
                }

            }else if(CATEGORY.equalsIgnoreCase(selectedItem)){
                String replacedQuery = getUpdatedQueryOfCateogories(graphQlQuery, response);
                if(replacedQuery != null){
                    return replacedQuery;
                }
            }else if(RESET_INVENTORY_BATCH.equalsIgnoreCase(selectedItem)){

                int resetInventory=Integer.parseInt(resetValue.getText());
                String retailerIdText = retailerId.getText();
                int retailerId = Integer.parseInt(retailerIdText);

                String replacedQuery = getResetUpdatedQueryOfInventoryPositions(graphQlQuery, response,resetInventory,retailerId);
                if (replacedQuery != null)
                    return replacedQuery;

            }

        }
        return StringUtils.EMPTY;

    }
    private VirtualPositions transformAndAddToVirtualPositionsList(JsonObject jsonObject) {
        Gson gson = new Gson();

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        VirtualPositions virtualPositions = data.getVirtualPositions();
        virtualPositions.getEdges().stream().forEach(edge -> {
            populateVirtualPostion(edge);
        });
        return virtualPositions;

    }
    private String getUpdatedQueryOfVirtualPositions(String graphQlQuery, JsonObject response) {
        VirtualPositions virtualPositions = transformAndAddToVirtualPositionsList(response);
        PageInfo pageInfo = virtualPositions.getPageInfo();
        if(pageInfo!= null){
            boolean hasNextPage = pageInfo.isHasNextPage();
            if(hasNextPage && CollectionUtils.isNotEmpty(virtualPositions.getEdges()) &&  virtualPositions.getEdges().size() > 0){
                int size = virtualPositions.getEdges().size();
                VirtualPositionEdge lastEdge = virtualPositions.getEdges().get(size - 1);
                // return the query if more entries to process
                String replacedQuery = getUpdatedQuery(graphQlQuery,lastEdge.getCursor());
                return replacedQuery;
            }
        }
        return null;
    }

    private String getUpdatedQueryOfStandardProducts(String graphQlQuery, JsonObject response) {
        StandardProducts standardProducts = transformAndAddToStandardProductsList(response);
        PageInfo pageInfo = standardProducts.getPageInfo();
        if(pageInfo!= null){
            boolean hasNextPage = pageInfo.isHasNextPage();
            if(hasNextPage && CollectionUtils.isNotEmpty(standardProducts.getEdges()) &&  standardProducts.getEdges().size() > 0){
                int size = standardProducts.getEdges().size();
                StandardProductEdge lastEdge = standardProducts.getEdges().get(size - 1);
                // return the query if more entries to process
                String replacedQuery = getUpdatedQuery(graphQlQuery,lastEdge.getCursor());
                return replacedQuery;
            }
        }
        return null;
    }
    private String getUpdatedQueryOfLocations(String graphQlQuery, JsonObject response) {
        Locations locations = transformAndAddToLocationsList(response);
        PageInfo pageInfo = locations.getPageInfo();
        if(pageInfo!= null){
            boolean hasNextPage = pageInfo.isHasNextPage();
            if(hasNextPage && CollectionUtils.isNotEmpty(locations.getEdges()) &&  locations.getEdges().size() > 0){
                int size = locations.getEdges().size();
                LocationEdge lastEdge = locations.getEdges().get(size - 1);
                // return the query if more entries to process
                String replacedQuery = getUpdatedQuery(graphQlQuery,lastEdge.getCursor());
                return replacedQuery;
            }
        }
        return null;
    }
    private String getUpdatedQueryOfCateogories(String graphQlQuery, JsonObject response) {
        Categories categories = transformAndAddToCategooriesList(response);
        PageInfo pageInfo = categories.getPageInfo();
        if(pageInfo!= null){
            boolean hasNextPage = pageInfo.isHasNextPage();
            if(hasNextPage && CollectionUtils.isNotEmpty(categories.getEdges()) &&  categories.getEdges().size() > 0){
                int size = categories.getEdges().size();
                CategoryEdge lastEdge = categories.getEdges().get(size - 1);
                // return the query if more entries to process
                String replacedQuery = getUpdatedQuery(graphQlQuery,lastEdge.getCursor());
                return replacedQuery;
            }
        }
        return null;
    }

    private StandardProducts transformAndAddToStandardProductsList(JsonObject jsonObject) {
        Gson gson = new Gson();

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        StandardProducts standardProducts = data.getStandardProducts();
        standardProducts.getEdges().stream().forEach(edge -> {
            populateStandardProduct(edge);
        });
        return standardProducts;

    }
    private Locations transformAndAddToLocationsList(JsonObject jsonObject) {
        Gson gson = new Gson();

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        Locations locations = data.getLocations();
        locations.getEdges().stream().forEach(edge -> {
            populateLocation(edge);
        });
        return locations;

    }
    private Categories transformAndAddToCategooriesList(JsonObject jsonObject) {
        Gson gson = new Gson();

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        Categories categories = data.getCategories();
        categories.getEdges().stream().forEach(edge -> {
            populateCategory(edge);
        });
        return categories;

    }
    private VariantProducts transformAndAddToVariantProductsList(JsonObject jsonObject) {
        Gson gson = new Gson();

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        VariantProducts variantProducts = data.getVariantProducts();
        variantProducts.getEdges().stream().forEach(edge -> {
            populateVariantProduct(edge);
        });
        return variantProducts;

    }

    private String getUpdatedQueryOfVariantProducts(String graphQlQuery, JsonObject response) {
        VariantProducts variantProducts = transformAndAddToVariantProductsList(response);
        PageInfo pageInfo = variantProducts.getPageInfo();
        if(pageInfo!= null){
            boolean hasNextPage = pageInfo.isHasNextPage();
            if(hasNextPage && CollectionUtils.isNotEmpty(variantProducts.getEdges()) &&  variantProducts.getEdges().size() > 0){
                int size = variantProducts.getEdges().size();
                VariantProductEdge lastEdge = variantProducts.getEdges().get(size - 1);
                // return the query if more entries to process
                String replacedQuery = getUpdatedQuery(graphQlQuery,lastEdge.getCursor());
                return replacedQuery;
            }
        }
        return null;
    }



    private String getUpdatedQueryOfInventoryPositions(String graphQlQuery, JsonObject response) {
        InventoryPositions inventoryPositions = transformAndAddToInventoryPositionsList(response);
        PageInfo pageInfo = inventoryPositions.getPageInfo();
        if(pageInfo!= null){
            boolean hasNextPage = pageInfo.isHasNextPage();
            if(hasNextPage && CollectionUtils.isNotEmpty(inventoryPositions.getEdges()) &&  inventoryPositions.getEdges().size() > 0){
                int size = inventoryPositions.getEdges().size();
                InventoryPositionEdge lastEdge = inventoryPositions.getEdges().get(size - 1);
                // return the query if more entries to process
                 String replacedQuery = getUpdatedQuery(graphQlQuery,lastEdge.getCursor());
                 return replacedQuery;
            }
        }
        return null;
    }

    private String getResetUpdatedQueryOfInventoryPositions(String graphQlQuery, JsonObject response,int resetValue,int retailerId) {
        InventoryPositions inventoryPositions = transformResetAndAddToInventoryPositionsList(response,resetValue,retailerId);
        PageInfo pageInfo = inventoryPositions.getPageInfo();
        if(pageInfo!= null){
            boolean hasNextPage = pageInfo.isHasNextPage();
            if(hasNextPage && CollectionUtils.isNotEmpty(inventoryPositions.getEdges()) &&  inventoryPositions.getEdges().size() > 0){
                int size = inventoryPositions.getEdges().size();
                InventoryPositionEdge lastEdge = inventoryPositions.getEdges().get(size - 1);
                // return the query if more entries to process
                String replacedQuery = getUpdatedQuery(graphQlQuery,lastEdge.getCursor());
                return replacedQuery;
            }
        }
        return null;
    }


    private String getUpdatedQueryOfFulfilments(String graphQlQuery, JsonObject response) {
        Fulfilments fulfilments = transformAndAddToFulfilmentsList(response);
        PageInfo pageInfo = fulfilments.getPageInfo();
        boolean hasNextPage = pageInfo.isHasNextPage();
        if (hasNextPage) {
        int size = fulfilments.getEdges().size();
            FulfilmentEdge fulfilmentEdge = fulfilments.getEdges().get(size - 1);
            // return the query if more entries to process
            String replacedQuery = getUpdatedQuery(graphQlQuery, fulfilmentEdge.getCursor());
            return replacedQuery;
        }
        return null;
    }


    private String getUpdatedQueryOfOrders(String graphQlQuery, JsonObject response) {
        Orders orders = transformAndAddToOrderList(response);
        PageInfo pageInfo = orders.getPageInfo();
        int size = orders.getEdges().size();
        Edges lastEdge = orders.getEdges().get(size - 1);
        boolean hasNextPage = pageInfo.isHasNextPage();
        // return the query if more entries to process
        if (hasNextPage) {
            String replacedQuery = getUpdatedQuery(graphQlQuery, lastEdge.getCursor());
            return replacedQuery;
        }
        return null;
    }
    private void populateInventoryPosition(InventoryPositionEdge edge){
        InventoryPosition inventoryPositionNode = edge.getNode();
        inventoryPositionsList.add(inventoryPositionNode);
    }
    private void populateResetInventoryPosition(InventoryPositionEdge edge,int resetValue,int retailerId){
        InventoryPosition inventoryPositionNode = edge.getNode();
        inventoryPositionNode.setOnHand(resetValue);
        Entities  e = new  Entities();
        e.setLocationRef(inventoryPositionNode.getLocationRef());
        e.setQty(resetValue);
        e.setCorrectedQty(0);
        e.setReserveQty(0);
        e.setSkuRef(inventoryPositionNode.getProductRef());
        e.setLocationRef(inventoryPositionNode.getLocationRef());
        e.setRetailerId(retailerId);
        entities.add(e);
    }
    private void populateVariantProduct(VariantProductEdge edge){
        VariantProduct variantProduct = edge.getNode();
        variantProductList.add(variantProduct);
    }
    private void populateStandardProduct(StandardProductEdge edge){
        StandardProduct standardProduct = edge.getNode();
        standardProductList.add(standardProduct);
    }
    private void populateLocation(LocationEdge edge){
        Location location = edge.getNode();
        locationList.add(location);
    }
    private void populateCategory(CategoryEdge edge){
        Category category = edge.getNode();
        categoryList.add(category);
    }
    private void populateVirtualPostion(VirtualPositionEdge edge){
        VirtualPosition virtualPosition = edge.getNode();
        virtualPositionList.add(virtualPosition);
    }


    private void populateFulfilment(FulfilmentEdge edge){
        FulfilmentNode node = edge.getNode();
       /* Fulfilment fulfilment = FulfilmentMapper.INSTANCE.fulfilmentNodeToFulfilment(node);
        fulfilmentsList.add(fulfilment);*/
       fulfilmentsList.add(node);
    }
    private void populateOrder(Edges edge) {
            OrderNode node = edge.getNode();
           // Order order = OrderMapper.INSTANCE.OrdersNodeToOrder(node);
            ordersList.add(node);

    }

    private Fulfilments transformAndAddToFulfilmentsList(JsonObject jsonObject) {
        Gson gson = new Gson();

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        Fulfilments fulfilments = data.getFulfilments();

        fulfilments.getEdges().stream().forEach(edge -> {
            populateFulfilment(edge);
        });
        return fulfilments;

    }

    private InventoryPositions transformAndAddToInventoryPositionsList(JsonObject jsonObject) {
        Gson gson = new Gson();
        // JsonElement jsonOrders = jsonObject.get("orders");

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        InventoryPositions inventoryPositions = data.getInventoryPositions();

        inventoryPositions.getEdges().stream().forEach(edge -> {
           populateInventoryPosition(edge);
        });
        return inventoryPositions;

    }
    private InventoryPositions transformResetAndAddToInventoryPositionsList(JsonObject jsonObject,int resetValue,int retailerId) {
        Gson gson = new Gson();
        // JsonElement jsonOrders = jsonObject.get("orders");

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        InventoryPositions inventoryPositions = data.getInventoryPositions();

        inventoryPositions.getEdges().stream().forEach(edge -> {
            populateResetInventoryPosition(edge,resetValue,retailerId);
        });
        return inventoryPositions;

    }


    private Orders transformAndAddToOrderList(JsonObject jsonObject) {
        Gson gson = new Gson();
        // JsonElement jsonOrders = jsonObject.get("orders");

        Response response = gson.fromJson(jsonObject, Response.class);
        Data data = response.getData();
        Orders orders = data.getOrders();

        orders.getEdges().stream().forEach(edge -> {
            populateOrder(edge);
        });
        return orders;

    }

    @FXML
    void onXtractClick(ActionEvent event) {
        downloadDir.clear();
        ordersList = new ArrayList<>();
        fulfilmentsList = new ArrayList<>();
        inventoryPositionsList = new ArrayList<>();
        variantProductList = new ArrayList<>();
        standardProductList = new ArrayList<>();
        virtualPositionList = new ArrayList<>();
        locationList = new ArrayList<>();
        categoryList = new ArrayList<>();

        boolean validForm = validate();
        URL graphqlEndpoint = null;
        if (validForm) {
            xtractButton.setVisible(false);
            spinner.setVisible(true);

            try {
                graphqlEndpoint = new URL(hostUrl.getText());

                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + authToken.getText());
                String graphQlQuery = query.getText();
                GraphQLClient graphQLClient = new GraphQLClient(graphqlEndpoint, headers);
                JsonObject variables = new JsonObject();
                String replacedQuery = null;

                replacedQuery = getAllEntries(graphQLClient, graphQlQuery, variables);
                while (!replacedQuery.isEmpty()) {
                    replacedQuery = getAllEntries(graphQLClient, replacedQuery, variables);
                }
                String selectedEntity = entityType.getSelectionModel().getSelectedItem();

                if (replacedQuery.isEmpty()) {
                    if (CSV.equalsIgnoreCase(selectedFileFormat)) {
                        writeToCsv(selectedEntity);
                    } else if (JSON.equalsIgnoreCase(selectedFileFormat)) {
                        writeToJson(selectedEntity);
                    }
                }

            } catch (Exception e) {
                spinner.setVisible(false);
                String message = e.getMessage();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Unable to execute query");
                alert.setContentText(message);
                alert.showAndWait();
                e.printStackTrace();
            } finally {
                spinner.setVisible(false);
                xtractButton.setVisible(true);
                previousCursor = "";
                entityType.getSelectionModel().clearSelection();
                fileFormat.getSelectionModel().clearSelection();
                query.setText(DEFAULT_TEXT);
            }
        }
    }

    private void writeToJson(String entityType) throws Exception {
        Date date = new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("dd_MM_yyyy_HHmmss");
        String currentDate = sdf.format(date);

        ObjectMapper mapper = new ObjectMapper();
        String selectedDirPath = System.getProperty("user.home");
        Files.createDirectories(Paths.get(selectedDirPath + "/ayataXtract"));

        switch (entityType){
            case ORDER :
                String ordersDestinationPath =  selectedDirPath +"/ayataXtract/Orders_" + currentDate+ ".json";
                writeOrdersToJson(mapper, ordersDestinationPath);
                break;
            case FULFILMENT:
                String fulfilmentsDestinationPath =  selectedDirPath +"/ayataXtract/Fulfilments_" + currentDate+ ".json";
                writeFulfimentsToJson(mapper, fulfilmentsDestinationPath);

                // writeFulfilmentToJson(current,selectedDirPath);
                break;
            case INVENTORY_POSITION:
                String inventoryDestinationPath =  selectedDirPath +"/ayataXtract/InventoryPositions_" + currentDate+ ".json";
                writeInventoryPositionsToJson(mapper,inventoryDestinationPath);
                break;
            case VARIANT_PRODUCT:
                String variantDestinationPath =  selectedDirPath +"/ayataXtract/InventoryPositions_" + currentDate+ ".json";
                writeVariantProductsToJson(mapper,variantDestinationPath);
                break;
            case STANDARD_PRODUCT:
                String standardProducts =  selectedDirPath +"/ayataXtract/StandardProducts" + currentDate+ ".json";
                writeStandardProductsToJson(mapper,standardProducts);
                break;
            case VIRTUAL_POSITION:
                String virtualPositions =  selectedDirPath +"/ayataXtract/VirtualPositions" + currentDate+ ".json";
                writeVirtualPositionsToJson(mapper,virtualPositions);
                break;
            case LOCATIONS:
                String locations =  selectedDirPath +"/ayataXtract/Locations" + currentDate+ ".json";
                writeLocationsToJson(mapper,locations);
                break;
            case CATEGORY:
                String categories =  selectedDirPath +"/ayataXtract/Categories" + currentDate+ ".json";
                writeCategoriesToJson(mapper,categories);
                break;
            case RESET_INVENTORY_BATCH:
                writeResetInventoryPositionsToJson(mapper);
                break;


        }
    }
    private void writeVirtualPositionsToJson(ObjectMapper mapper, String fileLocation) throws IOException {
        File fout = new File(fileLocation);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (VirtualPosition variantProduct : virtualPositionList) {
            String objectAsJson = mapper.writeValueAsString(variantProduct);
            bw.write(objectAsJson);
            bw.newLine();
        }
        bw.close();
        downloadDir.setText(fileLocation);
        downloadDir.setVisible(true);
        virtualPositionList.clear();
    }
    private void writeStandardProductsToJson(ObjectMapper mapper, String fileLocation) throws IOException {
            File fout = new File(fileLocation);
            FileOutputStream fos = new FileOutputStream(fout);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            for (StandardProduct variantProduct : standardProductList) {
                String objectAsJson = mapper.writeValueAsString(variantProduct);
                bw.write(objectAsJson);
                bw.newLine();
            }
            bw.close();
            downloadDir.setText(fileLocation);
            downloadDir.setVisible(true);
            standardProductList.clear();
    }
    private void writeLocationsToJson(ObjectMapper mapper, String fileLocation) throws IOException {
        File fout = new File(fileLocation);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (Location location : locationList) {
            String objectAsJson = mapper.writeValueAsString(location);
            bw.write(objectAsJson);
            bw.newLine();
        }
        bw.close();
        downloadDir.setText(fileLocation);
        downloadDir.setVisible(true);
        locationList.clear();
    }
    private void writeCategoriesToJson(ObjectMapper mapper, String fileLocation) throws IOException {
        File fout = new File(fileLocation);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (Category category : categoryList) {
            String objectAsJson = mapper.writeValueAsString(category);
            bw.write(objectAsJson);
            bw.newLine();
        }
        bw.close();
        downloadDir.setText(fileLocation);
        downloadDir.setVisible(true);
        categoryList.clear();
    }

    private void writeVariantProductsToJson(ObjectMapper mapper, String fileLocation) throws IOException {
        File fout = new File(fileLocation);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (VariantProduct variantProduct : variantProductList) {
            String objectAsJson = mapper.writeValueAsString(variantProduct);
            bw.write(objectAsJson);
            bw.newLine();
        }
        bw.close();
        downloadDir.setText(fileLocation);
        downloadDir.setVisible(true);
        variantProductList.clear();
    }
    private void writeInventoryPositionsToJson(ObjectMapper mapper, String fileLocation) throws IOException {
        File fout = new File(fileLocation);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (InventoryPosition inventoryPosition : inventoryPositionsList) {
            String objectAsJson = mapper.writeValueAsString(inventoryPosition);
            bw.write(objectAsJson);
            bw.newLine();
        }
        bw.close();
        downloadDir.setText(fileLocation);
        downloadDir.setVisible(true);
        inventoryPositionsList.clear();
    }
    private void writeResetInventoryPositionsToJson(ObjectMapper mapper) throws IOException {

        Date date = new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("dd_MM_yyyy_HHmmss");
        String currentDate = sdf.format(date);

        int entriesPerFile = Integer.parseInt(maxEntries.getText());
        int start = 0;
        int end = entriesPerFile;
        String selectedDirPath = System.getProperty("user.home");


        for(int i=0;end<=entities.size();i++){
            if(start == entities.size()){
                inventoryPositionsList.clear();
                entities.clear();
                return;
            }

            List<Entities> subList = this.entities.subList(start, end);
            if(end + entriesPerFile  > entities.size()){
                start =end ;
                end = entities.size();
            }else if(start <= entities.size()){
                start = end +1;
                end = end + entriesPerFile;
            }

            String inventoryBatch ="{";
            inventoryBatch= inventoryBatch.concat("\n");

            inventoryBatch =inventoryBatch.concat("\"action\":\"UPSERT\",");
            inventoryBatch= inventoryBatch.concat("\n");
            inventoryBatch=inventoryBatch.concat("\"entityType\":\"INVENTORY\",");
            inventoryBatch=inventoryBatch.concat("\n");
            inventoryBatch=inventoryBatch.concat("\"entities\":");
            inventoryBatch= inventoryBatch.concat("\n");
            inventoryBatch = inventoryBatch.concat(subList.toString());
            inventoryBatch= inventoryBatch.concat("\n");
            inventoryBatch= inventoryBatch.concat("}");

            String fileLocation  =  selectedDirPath +"/ayataXtract/resetInventories_"+currentDate+"_"+i+".json";
            File f = new  File(fileLocation);
            f.createNewFile();

            RandomAccessFile reader = new RandomAccessFile(fileLocation, "r");

            RandomAccessFile stream = new RandomAccessFile(fileLocation, "rw");

            FileChannel channel = stream.getChannel();
            String value = inventoryBatch.toString();
            byte[] strBytes = value.getBytes();
            ByteBuffer buffer = ByteBuffer.allocate(strBytes.length);
            buffer.put(strBytes);
            buffer.flip();
            channel.write(buffer);
            stream.close();
            channel.close();
            // verify

            reader.close();

            // bw.close();
            downloadDir.setText(fileLocation);
            downloadDir.setVisible(true);
        }


    }

    private void writeFulfimentsToJson(ObjectMapper mapper, String fileLocation) throws IOException {
        File fout = new File(fileLocation);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (FulfilmentNode fulfilmentNode : fulfilmentsList) {
            String objectAsJson = mapper.writeValueAsString(fulfilmentNode);
            bw.write(objectAsJson);
            bw.newLine();
        }
        bw.close();
        downloadDir.setText(fileLocation);
        downloadDir.setVisible(true);
        ordersList.clear();
    }

    private void writeOrdersToJson(ObjectMapper mapper, String fileLocation) throws IOException {
        File fout = new File(fileLocation);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (OrderNode order : ordersList) {
            String objectAsJson = mapper.writeValueAsString(order);
            bw.write(objectAsJson);
            bw.newLine();
        }
        bw.close();
        downloadDir.setText(fileLocation);
        downloadDir.setVisible(true);
        ordersList.clear();
    }

    private void writeToCsv(String entityType) throws IOException, com.opencsv.exceptions.CsvDataTypeMismatchException, com.opencsv.exceptions.CsvRequiredFieldEmptyException {

        Date date = new Date();
        SimpleDateFormat sdf= new SimpleDateFormat("dd_MM_yyyy_HHmmss");
        String currentDate = sdf.format(date);

        String selectedDirPath = System.getProperty("user.home");
        Files.createDirectories(Paths.get(selectedDirPath + "/ayataXtract"));
        switch (entityType){
            case ORDER :
                String ordersPath =  selectedDirPath +"/ayataXtract/Orders_" + currentDate+ ".csv";
                writeToCSV(ordersPath);
                break;
            case FULFILMENT:
                String fulfillmentPath =  selectedDirPath +"/ayataXtract/Fulfilments" + currentDate+ ".csv";
                writeToCSV(fulfillmentPath);
                break;
            case INVENTORY_POSITION:
                String inventoryPositions =  selectedDirPath +"/ayataXtract/InventoryPositions" + currentDate+ ".csv";
                writeToCSV(inventoryPositions);
                break;
            case VARIANT_PRODUCT:
                String variantProducts =  selectedDirPath +"/ayataXtract/VariantProducts" + currentDate+ ".csv";
                writeToCSV(variantProducts);
                break;
            case STANDARD_PRODUCT:
                String standardProducts =  selectedDirPath +"/ayataXtract/StandardProducts" + currentDate+ ".csv";
                writeToCSV(standardProducts);
                break;
            case VIRTUAL_POSITION:
                String virtualPositions =  selectedDirPath +"/ayataXtract/VirtualPositions" + currentDate+ ".csv";
                writeToCSV(virtualPositions);
                break;
            case LOCATIONS:
                String locations =  selectedDirPath +"/ayataXtract/Locations" + currentDate+ ".csv";
                writeToCSV(locations);
                break;
            case CATEGORY:
                String categories =  selectedDirPath +"/ayataXtract/categories" + currentDate+ ".csv";
                writeToCSV(categories);
                break;



        }

    }

    private void resetInventoryBatch(String fileLocation) throws Exception{
        if(CollectionUtils.isNotEmpty(inventoryPositionsList)){
            FileWriter writer = new  FileWriter(fileLocation);

        }

    }

    private void writeToCSV(String fileLocation) throws IOException, com.opencsv.exceptions.CsvDataTypeMismatchException, com.opencsv.exceptions.CsvRequiredFieldEmptyException {
        FileWriter writer = new
                FileWriter(fileLocation);


        // Write list to StatefulBeanToCsv object
        if(CollectionUtils.isNotEmpty(ordersList)){
            StatefulBeanToCsvBuilder<OrderNode> builder =
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv<OrderNode> beanWriter = builder.build();
            beanWriter.write(ordersList);
            ordersList.clear();

        }
        if(CollectionUtils.isNotEmpty(inventoryPositionsList)){
            StatefulBeanToCsvBuilder<InventoryPosition> builder =
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv<InventoryPosition> beanWriter = builder.build();
            beanWriter.write(inventoryPositionsList);
            inventoryPositionsList.clear();

        }
        if(CollectionUtils.isNotEmpty(fulfilmentsList)){
            StatefulBeanToCsvBuilder<FulfilmentNode> builder =
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv<FulfilmentNode> beanWriter = builder.build();
            beanWriter.write(fulfilmentsList);
            fulfilmentsList.clear();

        }
        if(CollectionUtils.isNotEmpty(variantProductList)){
            StatefulBeanToCsvBuilder<VariantProduct> builder =
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv<VariantProduct> beanWriter = builder.build();
            beanWriter.write(variantProductList);
            variantProductList.clear();

        }
        if(CollectionUtils.isNotEmpty(standardProductList)){
            StatefulBeanToCsvBuilder<StandardProduct> builder =
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv<StandardProduct> beanWriter = builder.build();
            beanWriter.write(standardProductList);
            variantProductList.clear();

        }
        if(CollectionUtils.isNotEmpty(virtualPositionList)){
            StatefulBeanToCsvBuilder<VirtualPosition> builder =
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv<VirtualPosition> beanWriter = builder.build();
            beanWriter.write(virtualPositionList);
            virtualPositionList.clear();

        }
        if(CollectionUtils.isNotEmpty(locationList)){
            StatefulBeanToCsvBuilder<Location> builder =
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv<Location> beanWriter = builder.build();
            beanWriter.write(locationList);
            locationList.clear();

        }
        if(CollectionUtils.isNotEmpty(categoryList)){
            StatefulBeanToCsvBuilder<Category> builder =
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv<Category> beanWriter = builder.build();
            beanWriter.write(categoryList);
            categoryList.clear();

        }

        // closing the writer object
        writer.close();
        downloadDir.setText(fileLocation);
        downloadDir.setVisible(true);
    }



    private String getUpdatedQuery(String graphQlQuery ,String newCursor) {
        // means srunning first time, and not found
        if(newCursor != null){
            if (previousCursor == null) {
                previousCursor = "";
            }
            String oldCursor = "after:\"" + previousCursor + "\"";
            String replaceNewCursor = "after:\"" + newCursor+"\"";
            String replacedQuery = graphQlQuery.replace(oldCursor, replaceNewCursor);
            previousCursor = newCursor;
            return replacedQuery;
        }
        return null;
    }

    private boolean validate() {
        RequiredFieldValidator validator = new RequiredFieldValidator();
        validator.setMessage("Please Provide Input");

        authToken.getValidators().add(validator);
        boolean isAuthTokenValid = authToken.validate();

        hostUrl.getValidators().add(validator);
        boolean isHostUrlValid = hostUrl.validate();

        entityType.getValidators().add(validator);
        boolean isValidEntityType = entityType.validate();

        fileFormat.getValidators().add(validator);
        boolean isValidFormat = fileFormat.validate();

        if (isAuthTokenValid &&
                isHostUrlValid && isValidEntityType && isValidFormat) {
            return true;
        }
        return false;

    }

    @FXML
    void onReset(ActionEvent event) {

        authToken.clear();

        entityType.getSelectionModel().clearSelection();
        fileFormat.getSelectionModel().clearSelection();

        query.setText(DEFAULT_TEXT);
        hostUrl.clear();
        maxEntries.clear();

        downloadDir.clear();
        downloadDir.setVisible(false);


    }


}
