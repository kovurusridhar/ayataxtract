package com.ayata.xtract;

public class Constants {
    public static final String DEFAULT_TEXT="Please select an entity type, which will provide default query and can be modified to your choice, also select file format to extract in desired choice\n" +
            "NOTE: Do not remove the after and pageinfo sections from query.";
    public static final String CSV = "CSV";
    public static final String JSON ="JSON";
    public static final String ORDER="Order";
    public static final String FULFILMENT="Fulfilment";
    public static final String VIRTUAL_POSITION="Virtual Position";
    public static final String INVENTORY_POSITION="Inventory Position";
    public static final String STANDARD_PRODUCT ="Standard Product";
    public static final String VARIANT_PRODUCT="Variant Product";
    public static final String LOCATIONS ="Locations";
    public static final String CATEGORY ="Category";
    public static final String DEFAULT_SANBOX_HOST ="https://ACCOUNT_ID.sandbox.api.fluentretail.com/graphql";
    public static final String DEFAULT_PROD_HOST ="https://ACCOUNT_ID.api.fluentretail.com/graphql";
    public static final String MAC_OS="Mac OS X";
    public static final String RESET_INVENTORY_BATCH ="Reset Inventory Batch";


}
