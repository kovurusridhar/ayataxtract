package com.ayata.xtract;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.awt.*;
import java.lang.reflect.Method;
import java.net.URL;

import static com.ayata.xtract.Constants.MAC_OS;



public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("ayataXtract.fxml"));
        Scene scene = new Scene(root, 1100, 700);
        String os = System.getProperty("os.name");
        if(MAC_OS.equalsIgnoreCase(os)){
            String javaVersion = System.getProperty("java.version");
            Class util = Class.forName("com.apple.eawt.Application");
            Method getApplication = util.getMethod("getApplication", new Class[0]);
            Object application = getApplication.invoke(util);
            Class params[] = new Class[1];
            params[0] = java.awt.Image.class;
            Method setDockIconImage = util.getMethod("setDockIconImage", params);
            URL url = Main.class.getClassLoader().getResource("dock.png");
            java.awt.Image awtImage = Toolkit.getDefaultToolkit().getImage(url);
            setDockIconImage.invoke(application, awtImage);

        }
        primaryStage.setTitle("AyataCommerce Xtract");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("dockImage3.png"));
        primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
